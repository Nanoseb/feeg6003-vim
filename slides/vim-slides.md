<!-- $theme: gaia -->
<!-- $size: 16:9 -->
<!-- template: invert -->


### Download the Virtual Machine from

http://www.southampton.ac.uk/%7Engcmbits/virtualmachines/feeg6003-vim.ova

###### Username and password: ==feeg6003==

Once the VM is launched, make sure your keyboard is properly setup:
Keyboard settings are accessible with the bottom left icon called ==Keyboard==, then go to the ==Layout== tab, and select ==Edit== if needed.


### Find the blog post at

https://computationalmodelling.bitbucket.io/tools/vim-basics.html


---

# VIM 

#### **==FEEG6003 Workshop==**

###### Ben Guthrie and S&eacute;bastien Lemaire


###### *05/03/2018*

---
<!-- page_number: true -->

# What is VIM?

**VIM** is a text editor

- ==**Why use VIM?**==
	- Ubiquitous
	- Highly configurable
	- Powerful and efficient

- ==**What can you use VIM for?**==
	- Simple text editor :arrow_right: full IDE

---

# Demonstration

---

# Overview of the Workshop

1. **VIM** basics, config files
2. Simple commands, syntax and navigation
3. More advanced commands and improved navigation
4. **VIM** Golf
4. Command line & visual mode
5. Plugins

---
<!-- template: default -->

# Setting up your vim environment

Navitage to home directory
```vim
cd ~
```

Setup the environment
```py
goto_level 1
```

---

# Opening and closing files

Create/open a file called **filename**
```vim
vim filename
```

Insert some text  
```
i
[INSERT TEXT]
<ESC>
```

Close the file
```py
:wq # Save and exit (alternatively ZZ)
:q! # Discard changes
```

---
<!-- template: invert -->
# Normal and Insert modes

#### ==Normal Mode==
- **VIM** starts in normal mode
- Must be in normal mode to use ==commands==
- If in doubt, `<ESC>`

#### ==Insert mode==

- Allows inserting text, but can't use commands
- Can be entered in a number of ways

---
<!-- template: default -->
# Entering insert mode
```vim
vim insert_mode.txt
```
Try entering insert mode by the following commands:
```py
i # Insert before the cursor
a # Append after cursor
o # Insert on a new line below the cursor
```
```py
I # Insert at the end beginning of the line
A # Append to the end of the line
O # Insert on a new line above the cursor
```
Remember, press `<ESC>` to return to normal mode.

---
<!-- template: invert -->
# Navigation
All navigation is done using the keyboard.

| Command | Movement |
| :-- | :-- |
| `h` `j` `k` `l` | Left, down, up, right |
| `w` | To the start of the next word |
| `W` | To the start of the next WORD |
| `0` `$` | To the start / end of the line |
| `(` `)` | Forward / backward one sentence |
| `{` `}` | Forward / backward one paragraph


---
<!-- template: default -->

### Try that now

```vim
vim navigation.py
```
Move left, down, up, right with `h` `j` `k` `l`.

Navigate between words with `w`, `b`, `e`.

Move to the start/end of a line with `0` or `$`.

Navigate between sentences/paragraphs with `(` `)` `{` `}`.

Move to the top/bottom of the file with `gg`, `G`.


---
<!-- template: invert -->

# VIM Commands

- **VIM** has many commands to enable efficient text editing
- Some are rarely used, others will be used all the time
- ==Complex commands can be formed by combining operators==
	- In the form: `verb` `modifier` `noun`.
- Must be in normal mode

---
<!-- template: default -->
# Command syntax examples
```vim
goto_level 2
vim commands.txt
```

```py
d2w # Delete 2 words
yis # Yank (copy) inside sentence
ct{character} # Change up to the next {character}
dW # delete (1) WORD
```
To perform commands on a whole line:
```py
dd # Delete line
yy # Copy line
S # Change the entire line
5dd # Delete 5 lines
```

---

### Now do the exercises in level 2

Here are some useful commands for reference:

|Verbs|Modifiers|Nouns|
|:--|:--|:--|
|`d`: *delete (and copy)* | `1`,`2`,`3`... | `s`: *sentence* |
|`c`: *change* | `i`: *inside* | `p`: *paragraph* |
|`y`: *yank (copy)* | `a`: *around* | `w`: *word* |
| | `t`: *to char* | `'`, `"`, `(`, `<`, ... |

`x`: *delete the character under the cursor*
`u`, `Ctrl-r`: *undo / redo*
`p` : *paste after cursor*
`.`: *repeat the last command*

---

<!-- template: invert -->

# Config file

- The ==.vimrc== file contains optional runtime configuration settings, e.g.
	- Key mapping
	- Plugin management
	- Customisation

- To examine and edit the config file:
```vim
vim .vimrc
```

---

# More advanced navigation

- ==Modifiers== can be placed before navigation commands
	- E.g. `2w`: *Move forward two words*

- Go to a specific line
	- `:10` or `10G`: *Go to line 10*


- Searching
	- In **VIM**, you can search for ==characters or strings==
	- When combined with **VIM** commands, this can enable rapid editing of an entire file

---

<!-- template: default -->

# Searching for strings
```vim
goto_level 3
vim searching.c
```

Search for all occurances of a certain string:
```python
/{string} # Find all occurances of {string}
* # Find all occurances of the word under the cursor
:%s/old/new/g # Replace all occurances of old with new
```
Jump between occurances of the string:
```python
n # Jump to next
N # Jump to previous
```

---

### Using all we have covered so far, try the exercises in level 3


Here is a recap of the search commands:

| Command | Definition |
|:--|:--|
| `/{str}` | Search for all occurances of a string |
| `*` | Search for the word under the cursor |
| `n`, `N` | Jump between found words |
| `:%s/old/new/g` | Replace all occurances of old with new |

---

# VIM Golf

Open the file vim_golf.txt
```vim
vim vim_golf.txt
```

The goal is to change the second line to look like the first, in as few key presses as possible.

==How many ways can you find to do it?==

---

<!-- template: invert -->
# More advanced usage

---

# VISUAL mode

We have seen ==Normal== and ==Insert mode==.

#### ==Visual (block) mode==

- Run commands and functions only on the selected text
- Can select lines, words, characteres, blocks 

---
# VISUAL mode

Enter in visual mode:
| Command | Definition |
|:--|:--|
| `v` | Character based selection |
| `V` | Whole line selection |
| `ctrl+v` | Block based selection |
Use displacement keystroke to expand the selection:
`h` `j` `k` `l`, `$` `0`, `(` `)`, `G` `gg` ...
Use commands on the selected text like: `y`, `d`, `x`, `I` ...


---

<!-- template: default -->
# Try challenges in level 4

| Command | Definition |
|:--|:--|
| `v` | Character based selection |
| `V` | Whole line selection |
| `ctrl+v` | Block based selection |

- To insert text on several lines at the same times:
`ctrl+v` to enter in a block based selection
`5j` for example to expand your selection
`I` to enter in insert mode
`what ever you want to add`
`<ESC>` to go back to ==normal mode==

---
<!-- template: invert -->
# Command line `:`

Functions can be run from a command prompt with `:`
##### File opening
- In the current window `:e path/to/file`
- In a new tab `:tabe path/to/file`

##### Text `s`ubstitution
Uses regex (like `sed`)
```py
:s/old/new/    # act on the current line/selection
:%s/old/new/   # act on the entire file
```

---
<!-- template: invert -->
# Command line `:`

##### Run bash commands

| Command | Definition |
|:--|:--|
|`:r file`          | add (read) external file inside the current one|
|`:r ! bash_command` | add output of bash command |
|`:! command `      | filter selection with command |


Example: 
```
:r ! ls 
:! sort  # alphabetical sort of lines (-R for random)
:! bc    # computes simple calculus using bc
```

---
<!-- template: default -->
### Try the exercises in level 5

```vim
vim
```

load the first task file with `:e task1-4.vim`
load the second file in a tab with `:tabe task5.py`
use `gt` to switch between tabs

| Command | Definition |
|:--|:--|
| `:s/old/new/g`| string substitution |
| `:r file`          | add (read) exteral file |
| `:r ! bash_command` | add output of bash command |
| `:! command `      | filter selection with command |

---
<!-- template: invert -->
# Plugins

A quite large quantity of plugin available for **VIM**.

For example:
- python pep8 check, and other language specific extections
- file broswer inside vim
- ctag view
- autocompletion
- anything else you can imagine ...

---
# Plugins
Plugin manager is preferable to easily install and remove them, pathogen is one of them
##### Pathogen
- 1 line in the `.vimrc`, and 1 file in `~/.vimautoload`
- To install a plugin just copy it to `~/.vim/bundle`

---
<!-- template: default -->
# Plugin installation

Let's install today's plugin:
- Have a look at its structure/code: 
```bash
 cd ~/.headerplugin
 tree 
 vim -p ~/.headerplugin/plugin/*        # "-p" to open all files in new tabs
```
- One liner installation:
```
cp -r ~/.headerplugin ~/.vim/bundle/headerplugin
```
Try it with `:WriteHeader` for example (or `:W`).

---
<!-- template: invert -->
# Plugin selection

- NerdTree: github.com/scrooloose/nerdtree
- Tagbar: github.com/majutsushi/tagbar
- Traces: github.com/xtal8/traces.vim
- Supertab: github.com/ervandew/supertab
- Syntastic: github.com/vim-syntastic/syntastic
- Headerplugin: ~/.headerplugin


---
<!-- template: default -->
### Plugin selection

Let's give them a try:
```
goto_level 6
ls ~/.vim/bundle
```
- traces: highlights search and replace regex
- nerdtree: `[ctrl]+n`
- tagbar: `[ctrl]+t`
- supertab: `[tab]`
- headerplugin: `:WriteHeader`, `:ToggleComment`

Make sure you have a look at your `~/.vimrc`

--- 
<!-- template: invert -->
# Further learning
#### Games/interactive tutorials
- Vim Adventures: vim-adventures.com
- Vim Golf: vimgolf.com
- Vim Genius: vimgenius.com
#### Documentation
- Vim Tips wiki: vim.wikia.com
- Vim cheat sheet: vim.rtorr.com
- Complete tutorial: danielmiessler.com/study/vim


--- 

# Bonus: 
## Vim keybinding everywhere else

- Firefox/chrome: ==vimium==
- Eclipse: ==vimwarp==
- Sublime: ==actualvim==
- atom: ==vim-mode-plus==
- Visual studio: ==vsvim==
- Evince: `hjkl` works by default
- vimdiff