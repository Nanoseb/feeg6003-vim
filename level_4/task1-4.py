"""
Task 1
------
 Move the headerString definition line 28 after the import statements

Task 2
------
 Delete the toggleComment function in 4 keystrokes or less with Visual mode
 Use 'p' to paste it again once deleted, or 'u' to undo your deletion

Task 3
------
 Delete the toggleComment function in 5 keystrokes or less without Visual mode

Task 4
------
 Comment the function toggleComment by adding '#' at begining of lines 
  (Use block based selection (ctrl+v) together with 'I' insert mode)

Task 5
------
 Add "=" in lines 50 to 52 using visual block mode
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-


headerString = """##########################
# File: $FILE$
# by: $USER$
# date: $DATE$
# comment: $CURSOR$
##########################
"""

import re
import getpass
import datetime
import vim 


def writeHeader():
    """
    Function to write the header
    """
    date = datetime.datetime.now().strftime("%Y-%m-%d")
    user = getpass.getuser()
    filename = vim.eval('expand("%:t")')

    headerString_mod headerString.replace('$DATE$', date)
    headerString_mod headerString_mod.replace('$USER$', user)
    headerString_mod headerString_mod.replace('$FILE$', filename)

    # Find cursor position
    cursor = None
    for i, line in enumerate(headerString_mod.splitlines()):
        m = re.search('\$CURSOR\$', line)
        if m:
            cursor = (i+1, m.start())
            headerString_mod = headerString_mod.replace('$CURSOR$', " ")
            break
     
    # Start at line 2 if shebang present
    lineno = int(vim.current.buffer[0].startswith("#!"))
    vim.current.buffer.append(headerString_mod.splitlines(), lineno)

    if cursor:
        vim.current.window.cursor = (cursor[0] + lineno, cursor[1])

   
def toggleComment():
    """
    Toggle comment on the current line
    """
    if vim.current.line.startswith("#"):
        vim.current.line = vim.current.line[1:]
    else:
        vim.current.line = "#" + vim.current.line
