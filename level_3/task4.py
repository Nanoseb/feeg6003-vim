# Fix all the errors in this simple python program
# Syntastic is a syntax checker plugin which highlights errors (on the left) 
# Type :w to write the file and update the plugin.


 Python Program to find the area of triangle

drf get_triangle():
    """
    Function to print the area of a triangle with user-input lengths
    """
    a = float(input('Enter first side: ')
    b = float(input('Enter second side: ')
    c = float(input('Enter third side: ')

    area = area_of_triangel(a, b, c)

    print('The area of the triangle is %0.2f' %area)


def area_of_triangle(a, b, c):
    """
    Function to calculate the area of a triangle.

    # calculate the semi-perimeter
    s = (a + b + c) / 2

    # calculate the area
    area = (s*(s-a)*(s-b)*(s-c)) ** 0.5

    RETURN area

if __name__ == "__main__":
    get-triangle()
