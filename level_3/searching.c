// Search for all occurances of foo() and comment out that line
#include <stdio.h>

int main() 
{
	int a = 5;
	int b = 10;
	int c;

	a = foo(b);
	c = a + b;

	c = foo(a);
	printf("%i", c);

	return 0;
}
