" LEVEL 3 TASKS
" -------------
"
" Task 1
" ------
" Search for writeHeader and append () 
"		writeHeader()
"
" Task 2
" ------
" Substitute all occurances of UnusedFunction with ToggleComment
"
" Task 3
" ------
" Append () to the end of any line that starts with command!
"		command! ToggleComment call ToggleComment()  ... 
"
" -- You can use A to append, n to move to the next occurance then . to repeat the last
"  command (e.g. inserting text).
" 
" --------------------------------
" Add the plugin to the path
" --------------------------------
python import sys
python import vim
python sys.path.append(vim.eval('expand("<sfile>:h")'))
python from headerplugin import *


" ---------------------------
"  Functions
" ---------------------------

function! WriteHeader()
python << endPython

writeHeader

endPython
endfunction


function! UnusedFunction()
python << endPython

toggleComment()

endPython
endfunction

" ----------------------------
"  Expose commands to the user
" ----------------------------

command! UnusedFunction call UnusedFunction
command! WriteHeader call WriteHeader
command! ExampleFunction call ExampleFunction

"nmap <C-e> :ToggleComment<CR>
