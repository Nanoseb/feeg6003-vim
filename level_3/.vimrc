
" Pathogen (plugin manager)
execute pathogen#infect()				   

" Syntastic (syntax checker plugin) settings
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_lis = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" to have background color working properly
set t_ut=

colorscheme gruvbox                        " set the color scheme
syntax enable                              " enable syntax processing
set background=dark                        " dark background
set mouse=a                                " enable the use of the mouse
set whichwrap=b,s,<,>,[,]                  " traverse line break
set tabstop=4                              " number of visual spaces per TAB
set softtabstop=4                          " number of spaces in tab when editing
set expandtab                              " tabs are spaces

set number                                 " show line numbers
set showcmd                                " show command in bottom bar
set cursorline                             " highlight current line
set smartcase                              " case insensitive smart

set whichwrap+=h,l                          " h and l navigate between lines

filetype indent on                         " load filetype-specific indent files
filetype plugin indent on
set wildmenu                               " visual autocomplete for command menu
set showmatch                              " highlight matching [{()}]

" Search
set incsearch                              " search as characters are entered
set hlsearch                               " highlight matches

" Clipboard
set clipboard=unnamedplus

" Remember info about open buffers on close
set viminfo^=%

" j and k natural mouvement on long wrapped lines
map j gj
map k gk

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7 


" remap jk to <ESC> 
inoremap jk <esc>                          

set wildignore+=*.pyc,*.pyo

" ------------------------- Status line -----------------------------------
hi User1 ctermbg=245 ctermfg=235 
hi User2 ctermbg=235 ctermfg=245 
hi User3 ctermbg=235 
hi User4 ctermbg=245 ctermfg=235 
hi User5 ctermbg=245 ctermfg=240 

set laststatus=2
set statusline=
set statusline+=%5*
set statusline+=\ %.20{expand(\"%:~:h\")}/%1*
set statusline+=%{expand(\"%:t\")}
set statusline+=%m
set statusline+=%h 
set statusline+=\ %3*
set statusline+=%=
set statusline+=\ %l\/%L



