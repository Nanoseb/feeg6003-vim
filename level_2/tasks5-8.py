"""
TODO
    Task 5
    ------
    Append brackets () to the end of line 46 -- recall A appends to the end of a line
            date = datetime.datetime.now()
    Navigate to the line below and press the period key . to repeat the last command

    Task 6
    ------
    Copy line 51 and paste below, then change everything inside the brackets to
    ('$FILE$', filename)
            headerString_mod = headerString_mod.replace('$FILE$', filename)

    Task 7
    ------
    On line 36, change USERNAME to USER by moving the cursor to the N and deleting to the end of the word (dw) 

    
    Task 8
    ------
    Yank line 31 (yy), then paste 3 times (3p). Change the imported names to:
    import re
    import getpass
    import datetime
    import vim 

"""


import re


headerString = """##########################
# File: $FILE$
# by: $USERNAME$
# date: $DATE$ 
# comment: $CURSOR$
##########################
"""

def writeHeader():
    """
    Function to write the header
    """
    date = datetime.datetime.now
    user = getpass.getuser
    filename = vim.eval('expand("%:t")')

    headerString_mod = headerString.replace('$DATE$', date)
    headerString_mod = headerString_mod.replace('$USER$', user)

    # Find cursor position
    cursor = None
    for i, line in enumerate(headerString_mod.splitlines()):
        m = re.search('\$CURSOR\$', line)
        if m:
            cursor = (i+1, m.start())
            headerString_mod = headerString_mod.replace('$CURSOR$', " ")
            break
     
    # Start at line 2 if shebang present
    lineno = int(vim.current.buffer[0].startswith("#!"))
    vim.current.buffer.append(headerString_mod.splitlines(), lineno)

    if cursor:
        vim.current.window.cursor = (cursor[0] + lineno, cursor[1])

