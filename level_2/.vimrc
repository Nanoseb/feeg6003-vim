" Remap ESC to jk "  
inoremap jk <ESC> 

" Enable syntax processing "
syntax enable

" Number of visual spaces per tab "
set tabstop=4
" Number of spaces inserted in tab when editing "
set softtabstop=4

" Show line numbers "
set number
