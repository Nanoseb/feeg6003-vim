" LEVEL 1 TASKS
"
" Task 1
" ------
" 1. Copy line 31 using (yy)
"		python import sys
" 2. Paste (p) the line below
" 3. Navigate to the word sys and change the word (cw) to vim
"		python import vim 
"
" Complete the following tasks using a single command
"
" Task 2
" ------
" Go to line 39 and delete everything inside the parentheses
"		function! WriteHeader( params )
" 
" Task 3
" ------
" Go to the start of line 48 and delete the following 7 lines
"		function! UnusedFunction( params )
"
" Task 4
" ------
" Delete line 55 
"		command! UnusedFunction call UnusedFunction()
"
" Now open the file tasks5-8.py 


python import sys


" ---------------------------
"  Functions
" ---------------------------

function! WriteHeader( params )
python << endPython

writeHeader()

endPython
endfunction


function! UnusedFunction()
python << endPython

unusedFunction()

endPython
endfunction


" ----------------------------
"  Expose commands to the user
" ----------------------------

command! UnusedFunction call UnusedFunction()
command! WriteHeader call WriteHeader()
