" Task 1
" ------
"  Change every occurence of WriteHeader to PrintHeader 
"
" Task 2
" ------
"  Copy the content of function toggleComment from task5.py inside
"  ToggleComment (line 35), you can use the :tabe to open both file at the
"  same time
"
" Task 3
" ------
"  Comment out properly lines 57 to 59 with " instead of '-' using :s/...
"  commands
"  from - ------------------- to " -------------------
"
" Task 4
" ------
"  Delete all commented lines using :s/... command
"
"
" --------------------------------
" Add the plugin to the path
" --------------------------------
python import sys
python import vim
python sys.path.append(vim.eval('expand("<sfile>:h")'))
python from headerplugin import *


" ---------------------------
"  Functions
" ---------------------------

function! WriteHeader()
python << endPython

writeHeader()

endPython
endfunction


function! ToggleComment()
python << endPython

toggleComment()

endPython
endfunction

- ----------------------------
-  Expose commands to the user
- ----------------------------

command! ToggleComment call ToggleComment()
command! WriteHeader call WriteHeader()

"nmap <C-e> :ToggleComment<CR>
