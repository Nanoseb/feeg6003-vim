#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import getpass
import datetime
import vim 


headerString = """##########################
# File: $FILE$
# by: $USER$
# date: $DATE$ 
# comment: $CURSOR$
##########################
"""

def writeHeader():
    """
    Function to write the header
    """
    date = datetime.datetime.now().strftime("%Y-%m-%d")
    user = getpass.getuser()
    filename = vim.eval('expand("%:t")')

    headerString_mod = headerString.replace('$DATE$', date)
    headerString_mod = headerString_mod.replace('$USER$', user)
    headerString_mod = headerString_mod.replace('$FILE$', filename)

    # Find cursor position
    cursor = None
    for i, line in enumerate(headerString_mod.splitlines()):
        m = re.search('\$CURSOR\$', line)
        if m:
            cursor = (i+1, m.start())
            headerString_mod = headerString_mod.replace('$CURSOR$', " ")
            break
     
    # Start at line 2 if shebang present
    lineno = int(vim.current.buffer[0].startswith("#!"))
    vim.current.buffer.append(headerString_mod.splitlines(), lineno)

    if cursor:
        vim.current.window.cursor = (cursor[0] + lineno, cursor[1])

