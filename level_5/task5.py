"""
Task 5
------

Copy the content of the file task5_bis.py inside this file
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import vim 

   
def toggleComment():
    """
    Toggle comment on the current line
    """
    if vim.current.line.startswith("#"):
        vim.current.line = vim.current.line[1:]
    else:
        vim.current.line = "#" + vim.current.line
