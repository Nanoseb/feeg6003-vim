# Task 7
# ------
#  Replace the lines with their filtered result

# Compute (use bc)
3*14+15*92

# List here elements from the current directory (ls),
# and sort them randomly (sort -R)


# Compute the md5 sum of the following passwort (md5sum)
password="In_Vim_We_Trust"


# Make this json pretty (use the command "jq ." or "python -m json.tool")
{"string":"Vim cheat sheet","command":["h","j","k","l","i"],"action":["left","down","up","right","insert mode"]}



# Decode the following message (base64 -d)
IyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC46OiEhISEhISE6LgojIC4hISEh
ITouICAgICAgICAgICAgICAgICAgICAgICAgLjohISEhISEhISEhISEKIyB+fn5+ISEhISEhLiAg
ICAgICAgICAgICAgICAgLjohISEhISEhISFVV1dXJCQkIAojICAgICA6JCROV1ghITogICAgICAg
ICAgIC46ISEhISEhWFVXVyQkJCQkJCQkJFAgCiMgICAgICQkJCQkIyNXWCE6ICAgICAgLjwhISEh
VVckJCQkIiAgJCQkJCQkJCQjIAojICAgICAkJCQkJCAgJCQkVVggICA6ISFVVyQkJCQkJCQkJCAg
IDQkJCQkJCogCiMgICAgIF4kJCRCICAkJCQkXCAgICAgJCQkJCQkJCQkJCQkICAgZCQkUiIgCiMg
ICAgICAgIiokYmQkJCQkICAgICAgJyokJCQkJCQkJCQkJG8rIyIgCiMgICAgICAgICAgICAiIiIi
ICAgICAgICAgICIiIiIiIiIgCg==

