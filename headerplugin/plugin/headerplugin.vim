" --------------------------------
" Add the plugin to the path
" --------------------------------
python3 import sys
python3 import vim
python3 sys.path.append(vim.eval('expand("<sfile>:h")'))
python3 from headerplugin import *


" ---------------------------
"  Functions
" ---------------------------

function! WriteHeader()
python3 << endPython

writeHeader()

endPython
endfunction


function! ToggleComment()
python3 << endPython

toggleComment()

endPython
endfunction

" ----------------------------
"  Expose commands to the user
" ----------------------------

command! ToggleComment call ToggleComment()
command! WriteHeader call WriteHeader()

"nmap <C-e> :ToggleComment<CR>
